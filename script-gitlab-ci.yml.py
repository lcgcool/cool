#!/usr/bin/env python3

def build_gitlabci(binary_tag, view, OS):
  name = f"build_{binary_tag}_{view}"
  return f"""{name}:
  variables:
    BINARY_TAG: {binary_tag}
    LCG_RELEASE_PATH: /cvmfs/sft-nightlies.cern.ch/lcg/views/{view}/latest/{binary_tag}
    CMAKE_ARGUMENTS: -DCMAKE_BUILD_TYPE=Debug -DLCG_python3=on -DBINARY_TAG={binary_tag}
  before_script:
    - "echo $BINARY_TAG $CI_COMMIT_REF_NAME"
    - "export highest=$(curl --header \\\"PRIVATE-TOKEN: $PRIVATE_TOKEN\\\" \\\"https://gitlab.cern.ch/api/v4/projects/25684/jobs?per_page=100\\\" | jq  \\\".[] | select(.name == \\\\\\\"{name}\\\\\\\") | select(.ref == \\\\\\\"${{CI_COMMIT_REF_NAME}}\\\\\\\") | .id\\\" | sort -n | tail -1)"
    - "echo $highest"
    - "curl -L --header \\\"PRIVATE-TOKEN: $PRIVATE_TOKEN\\\" \\\"https://gitlab.cern.ch/api/v4/projects/25684/jobs/$highest/artifacts\\\" > coral-{name}.zip"
    - "ls -l coral-{name}.zip"
    - "unzip coral-{name}.zip -d ./coral"
  stage: build 
  script: 
    - "source $LCG_RELEASE_PATH/setup.sh"
    - export CMAKE_PREFIX_PATH=./coral/ci_build:$CMAKE_PREFIX_PATH
    - "cmake $CMAKE_ARGUMENTS -S . -B ci_build"
    - "cmake --build ci_build"
    - export CORAL_AUTH_PATH=/home/gitlab-runner/
    - export CORAL_DBLOOKUP_PATH=/home/gitlab-runner/
    - export COOL_QMTEST_USER=sftnight
    - export COOLSYS=ci_build
    - "ctest --test-dir ci_build --output-on-failure"
    - "! grep -e Failed ci_build/Testing/Temporary/LastTest.log"
  tags: 
    - {OS}

"""

platforms=['dev3-x86_64-el9-gcc11-opt', 'dev3-x86_64-el9-gcc13-dbg', 'dev3-x86_64-el9-gcc13-opt']
 
with open('.gitlab-ci.yml', 'w') as f:
  f.write("""---
# Stages for the CI build.
stages:
  - build

# Set the behaviour of the CI build.
variables:
  GIT_STRATEGY: fetch
  GIT_SUBMODULE_STRATEGY: recursive

# Common settings for all of the jobs.
before_script:
  - export MAKEFLAGS="-j`nproc` -l`nproc`"

""")
  for platform in platforms:
    list = platform.split('-')
    f.write(build_gitlabci(platform[platform.find('-')+1:], list[0], list[2]))

