#!/bin/bash
echo Executing `cd $(dirname $0); pwd`/`basename $0`

# Check the input arguments
tests=""
if [ "$1" != "" ]; then
  while [ "$1" != "" ]; do
    if [ "$tests" = "" ]; then
      tests="$1"
    else
      tests="$tests $1"
    fi
    shift
  done
fi

# Locate the installation directory
###insdir=@CMAKE_INSTALL_PREFIX@

# This script is now installed in the install directory (CORALCOOL-2925)
insdir=`cd $(dirname $0); pwd`

# Locate the script to set up the environment
if [ ! -f $insdir/cc-run ]; then
  echo "ERROR! $insdir/cc-run does not exist"
  exit 1
fi
ccrun="$insdir/cc-run -q"

# Debug messages for AFS (NB: tokens hangs on mac, see SPI-925)
###echo "KERBEROS TICKETS"
###klist
###echo "AFS TOKENS"
###tokens
###echo ""

# Check for access to authentication.xml on AFS (e.g. Ubuntu, CORALCOOL-2762)
if [ -f /afs/cern.ch/sw/lcg/app/pool/db/authentication.xml ]; then
  quickTests=QUICK
else
  quickTests=QUICK_NO_AFS
fi

# See https://svnweb.cern.ch/trac/lcgsoft/browser/trunk/lcgcmt/LCG_Builders/CORAL/scripts/CORAL_test.sh
# See https://svnweb.cern.ch/trac/lcgsoft/browser/trunk/lcgcmt/LCG_Builders/COOL/scripts/COOL_test.sh
if [ -d $insdir/CoralTest/qmtest ]; then
  if [ "$tests" = "" ]; then
    ###tests=ALL
    tests=$quickTests
  fi
  cd $insdir/CoralTest/qmtest
elif [ -d $insdir/CoolTest/qmtest ]; then
  if [ "$tests" = "" ]; then
    ###tests=$COOL_QMTEST_TARGET
    tests=$quickTests
  fi
  cd $insdir/CoolTest/qmtest
else
  echo "ERROR! $insdir/CoralTest/qmtest and $insdir/CoolTest/qmtest not found"
  exit 1
fi

if [ -z ${WORKSPACE_TMP+x} ]; then #SPI-1724
  tmpdir=$(mktemp -d  --tmpdir=. $USER.XXXXXXXXXXXX)
else 
  tmpdir=$(mktemp -d  --tmpdir=${WORKSPACE_TMP} $USER.XXXXXXXXXXXX)
fi
export CORALCOOL_QMTEST_WORKDIR=$tmpdir
echo "Using CORALCOOL_QMTEST_WORKDIR=${CORALCOOL_QMTEST_WORKDIR}"
tmpout=$tmpdir/qmtestoutput
qmtestRun="qmtest run --no-output"
if [ `${ccrun} python -c 'import platform; print (platform.system())'` == "Darwin" ]; then qmtestRun="python `${ccrun} which qmtest` run"; fi # CORALCOOL-2884
echo Execute ${ccrun} ${qmtestRun} -f brief $tests
${ccrun} ${qmtestRun} -f brief $tests > $tmpout
status=$?
cat $tmpout
###echo "status[1]=$status"
if [ "$status" == "0" ]; then
  # qmtest run succeded - return SUCCESS
  status=0
else
  # qmtest run failed - check why
  sep="--- STATISTICS ---------------------------------------------------------------"
  grep -q -x -e "$sep" $tmpout
  status=$?
  ###echo "status[2]=$status"
  if [ "$status" != "0" ]; then
    # qmtest did not run (no qmtest, wrong test suite, ...) - return FAILURE
    status=2
  else
    # qmtest did run - check if ERROR/FAIL or only UNTESTED
    cat $tmpout | awk -v sep="$sep" 'BEGIN{out=0}{if ($0==sep) out=1; if (out==1) print}' | egrep -q "(FAIL|ERROR)"
    if [ "$?" == "0" ]; then
      # qmtest run produced ERROR and/or FAIL - return FAILURE
      status=1
    else
      # qmtest run did not produce ERROR and/or FAIL - return SUCCESS
      status=0
    fi
    ###echo "status[3]=$status"
  fi
fi
\rm -rf $tmpdir
if [ "$status" == "0" ]; then
  echo "Tests succeeded (status=$status)"
else
  echo "Tests failed (status=$status)"
fi
exit $status
