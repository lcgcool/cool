# - Find expat
# Defines:
#
#  EXPAT_FOUND
#  EXPAT_INCLUDE_DIR
#  EXPAT_LIBRARY
#  EXPAT_EXECUTABLE

find_path(EXPAT_INCLUDE_DIR NAMES expat.h)
find_library(EXPAT_LIBRARY NAMES expat)
find_program(EXPAT_EXECUTABLE NAMES xmlwf)
get_filename_component(EXPAT_LIBRARY_DIRS ${EXPAT_LIBRARY} PATH)
get_filename_component(EXPAT_BINARY_PATH ${EXPAT_EXECUTABLE} PATH)

# handle the QUIETLY and REQUIRED arguments and set EXPAT_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Expat DEFAULT_MSG EXPAT_INCLUDE_DIR EXPAT_LIBRARY EXPAT_EXECUTABLE)

mark_as_advanced(EXPAT_INCLUDE_DIR EXPAT_LIBRARY EXPAT_EXECUTABLE)


