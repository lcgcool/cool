# - Locate ffilibrary
# Defines:

#  FFI_FOUND
#  FFI_LIBRARIES
#  FFI_LIBRARY_DIRS (not cached)

find_library(FFI_LIBRARIES NAMES ffi)

get_filename_component(FFI_LIBRARY_DIRS ${FFI_LIBRARIES} PATH)

# handle the QUIETLY and REQUIRED arguments and set FFI_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(FFI DEFAULT_MSG FFI_LIBRARIES)

mark_as_advanced(FFI_FOUND FFI_LIBRARIES)
