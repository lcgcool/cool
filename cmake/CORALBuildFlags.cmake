# Build options
option(BUILD_SHARED_LIBS "Set to OFF to build static libraries." ON)

# Build flags (C++ std)
# [NB Keep flags passed from lcgcmake or setupLCG.sh (CORALCOOL-2854)]
# [NB The -std=c++11 flags must be passed from outside (CORALCOOL-2846)]

# Build flags (gcc-toolchain options for clang)
# [NB Keep flags passed from lcgcmake or setupLCG.sh (CORALCOOL-2854)]
# [NB The --gcc-toolchain flags must be passed from outside (CORALCOOL-2846)]

# Build flags (warnings and other common options)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic -Wall -Wextra -Wno-long-long -Wno-deprecated -pthread -fPIC")

# Build flags (other platform-dependent settings)
IF(CMAKE_HOST_SYSTEM_NAME MATCHES "Darwin")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
ELSEIF(BINARY_TAG MATCHES "gcc[^56].*")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wimplicit-fallthrough=1") # CORALCOOL-2871
ELSEIF(BINARY_TAG MATCHES "clang.*")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Qunused-arguments") # CORALCOOL-2871
ELSE()
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wl,--no-undefined")
ENDIF()

# Build flags (optional test for x86-like char behaviour also on ARM)
###set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsigned-char")

# Build flags (optional test for ARM-like char behaviour also on x86)
###set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -funsigned-char")

message(STATUS "Using CMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}")
