# Collection of helpers for CORAL test configuration
find_package(CppUnit)
include_directories(${CPPUNIT_INCLUDE_DIRS})

include(CMakeParseArguments)
function(coral_add_unit_test name)
  # coral_add_unit_test(name [PACKAGE package_name] [LIBS libs...])
  cmake_parse_arguments(ARG "" "PACKAGE" "LIBS" ${ARGN})
  file(GLOB ${name}_srcs ${CMAKE_CURRENT_SOURCE_DIR}/tests/${name}/*.cpp)
  if(NOT ${CMAKE_VERSION} VERSION_LESS 3.6.0)
    list(FILTER ${name}_srcs EXCLUDE REGEX "#")
  endif()
  if(ARG_PACKAGE)
    set(bin_name test_${ARG_PACKAGE}_${name})
    set(test_name ${ARG_PACKAGE}.${name})
  else()
    set(bin_name test_${name})
    set(test_name ${name})
  endif()
  #message(STATUS "Test test_${name} <- ${${name}_srcs} ${${name}_extra_srcs}")
  add_executable(${bin_name} ${${name}_srcs} ${${name}_extra_srcs})
  set_target_properties(${bin_name} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/tests/bin)
  target_link_libraries(${bin_name} ${ARG_LIBS} ${CPPUNIT_LIBRARIES})
  add_dependencies(POST_BUILD_BANNER ${bin_name})
  add_test(NAME ${test_name}
           COMMAND ${bin_name}
           WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/tests/${name})
endfunction()

# Copy and install python tests into the build and install areas
include(CORALConfigScripts)
macro(copy_and_install_python_test _name)
  if (LCG_python3 STREQUAL on) # CORALCOOL-2976
    copy_to_build(${_name} tests_python3 tests/bin)
  else()
    copy_to_build(${_name} tests tests/bin)
  endif()
endmacro()
