# Collection of helpers for CORAL script configuration/installation

# Copy and install scripts into the build and install areas
macro(copy_to_build _name _srcdir _blddir)
  add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/${_blddir}/${_name} COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${_srcdir}/${_name} ${CMAKE_BINARY_DIR}/${_blddir}/${_name} DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${_srcdir}/${_name} DEPENDS PRE_BUILD_BANNER)
  string(REGEX REPLACE "/" "_" _target "${_name}")
  add_custom_target(${_target} ALL DEPENDS ${CMAKE_BINARY_DIR}/${_blddir}/${_name})
  add_dependencies(${_target} PRE_BUILD_BANNER)
  add_dependencies(POST_BUILD_BANNER ${_target})
endmacro()

macro(copy_and_install_program _name _srcdir _blddir _insdir)
  copy_to_build(${_name} ${_srcdir} ${_blddir})
  get_filename_component(_dstdir ${_insdir}/${_name} PATH)
  install(PROGRAMS ${CMAKE_BINARY_DIR}/${_blddir}/${_name} DESTINATION ${_dstdir})
endmacro()

macro(copy_and_install_file _name _srcdir _blddir _insdir)
  copy_to_build(${_name} ${_srcdir} ${_blddir})
  get_filename_component(_dstdir ${_insdir}/${_name} PATH)
  install(FILES ${CMAKE_BINARY_DIR}/${_blddir}/${_name} DESTINATION ${_dstdir})
endmacro()
