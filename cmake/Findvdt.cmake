# - Locate vdtlibrary
# Defines:

#  VDT_FOUND
#  VDT_LIBRARIES
#  VDT_LIBRARY_DIRS (not cached)

find_library(VDT_LIBRARIES NAMES vdt)

get_filename_component(VDT_LIBRARY_DIRS ${VDT_LIBRARIES} PATH)

# handle the QUIETLY and REQUIRED arguments and set VDT_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(vdt DEFAULT_MSG VDT_LIBRARIES)

mark_as_advanced(VDT_FOUND VDT_LIBRARIES)
