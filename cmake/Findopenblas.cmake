# - Locate openblaslibrary
# Defines:

#  OPENBLAS_FOUND
#  OPENBLAS_LIBRARIES
#  OPENBLAS_LIBRARY_DIRS (not cached)

find_library(OPENBLAS_LIBRARIES NAMES openblas)

get_filename_component(OPENBLAS_LIBRARY_DIRS ${OPENBLAS_LIBRARIES} PATH)

# handle the QUIETLY and REQUIRED arguments and set OPENBLAS_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(openblas DEFAULT_MSG OPENBLAS_LIBRARIES)

mark_as_advanced(OPENBLAS_FOUND OPENBLAS_LIBRARIES)
