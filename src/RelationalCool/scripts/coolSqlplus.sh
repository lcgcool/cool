#!/bin/bash
rlwrap -h > /dev/null 2&>1
if [ $? == 0 ]; then
  rlwrap \sqlplus $@
else
  \sqlplus $@
fi

