#ifndef COOLKERNEL_VERSIONINFO_H
#define COOLKERNEL_VERSIONINFO_H 1

// Explicitly disable COOL4xx extensions (do not allow -D to enable them)
#undef COOL400TX // API exposing transactions (CORALCOOL-2707, CORALCOOL-2536)
#undef COOL400CPP11ENUM // API with c++11 enum class (CORALCOOL-2233)

// This switch is no longer needed in trunk (CORALCOOL-1111, CORALCOOL-2943)
//#define COOL300 1 // COOL 3.0 or higher (COOL 2.x is no longer supported!)

// This switch is no longer needed in trunk (CORALCOOL-1111, CORALCOOL-2943)
//#define COOL290 1 // COOL 2.9 or higher (COOL 2.8 is no longer supported!)

// This switch is no longer needed in trunk (CORALCOOL-2808, CORALCOOL-2809)
//#define COOL_HAS_CPP11 1 // COOL code uses c++11 

// COOL_VERSIONINFO_RELEASE is #defined in API as of COOL 2.8.4 (sr #111706)
// COOL_VERSIONINFO_RELEASE_x are #defined as of COOL 2.8.15
// Note that the former is defined within quotes, the latter are not!
#define COOL_VERSIONINFO_RELEASE_MAJOR 3
#define COOL_VERSIONINFO_RELEASE_MINOR 3
#define COOL_VERSIONINFO_RELEASE_PATCH 18
#define COOL_VERSIONINFO_RELEASE "3.3.18"

// Sanity check: does this compiler support c++11? (see CORALCOOL-2808)
#if ( ! defined(__GXX_EXPERIMENTAL_CXX0X__) ) && (__cplusplus < 201103L )
#error("ERROR: COOL >= 3.0.0 but this compiler does not support c++11")
#endif

// Drop support for TimingReport as of COOL 2.8.15 (CORALCOOL-2642)
#undef COOL_ENABLE_TIMING_REPORT

#endif // COOLKERNEL_VERSIONINFO_H
