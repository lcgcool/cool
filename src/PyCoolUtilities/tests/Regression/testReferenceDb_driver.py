#!/usr/bin/env python

###print 'DEBUG: Import os, sys, platform'
import os, sys, platform

###print 'DEBUG: Import subprocess'
from subprocess import Popen, PIPE, STDOUT

###print 'DEBUG: Import ReferenceDbMgr'
###from PyCoolReferenceDb import ReferenceDbMgr

###print 'DEBUG: Import TestReferenceDb1RO'
###from PyCoolReferenceDb import TestReferenceDb1RO

###print 'DEBUG: Import TestReferenceDb1RW'
###from PyCoolReferenceDb import TestReferenceDb1RW

###print 'DEBUG: Import TestReferenceDb2RO'
###from PyCoolReferenceDb import TestReferenceDb2RO

###print 'DEBUG: Import cool'
from PyCool import cool
serviceVersion290 = True
try: cool.PayloadMode()
except: serviceVersion290 = False

envKey  = "COOLTESTDB"
envKeyR = "COOLTESTDB_R"

##############################################################################

def usage() :
    print 'Usage: ' + os.path.basename(sys.argv[0]) + \
          ' [-noRefresh] [1.3.0|2.0.0|2.2.0|2.8.0|2.9.0]'
    print '[NB: You must set the environment variable %s]'%(envKey)
    print '[NB: Optionally you can set also %s]'%(envKeyR)
    sys.exit(-1)

##############################################################################

if __name__ == '__main__':

    # The noRefresh argument has no effect for createReferenceDb
    # (it is only relevant for TestReferenceDbXXX)
    noRefresh = False
    if len(sys.argv) > 1 and sys.argv[1] == "-noRefresh" :
        noRefresh = True
        del sys.argv[1]
    if len(sys.argv) > 2 or \
           ( len(sys.argv) == 2 and \
             sys.argv[1] not in ["1.3.0", "2.0.0", "2.2.0", "2.8.0", "2.9.0"] ):
        usage()
    elif len(sys.argv) == 2:
        refSchemaVersion = sys.argv[1]
        del sys.argv[1]
    else:
        if serviceVersion290: refSchemaVersion = "2.9.0"
        else: refSchemaVersion = "2.8.0"

    # Check environment variables
    if envKey in os.environ:
        connectString = os.environ[envKey]
        print 'DEBUG:', envKey, '->', connectString
        if envKeyR in os.environ:
            connectStringR = os.environ[envKeyR]
            print 'DEBUG:', envKeyR, '->', connectStringR
            if connectString != connectStringR:
                print 'DEBUG: Environment variables', envKey, 'and', envKeyR,\
                      'have different values: assume that',\
                      'the R/O and R/W tests will use different replicas'
            else:
                print 'DEBUG: Environment variables', envKey,\
                      'and', envKeyR, 'have the same value:'\
                      'will check replicas for', connectString
        else:
            connectStringR = connectString
            print 'DEBUG: Environment variable', envKeyR, 'is not defined:',\
                  'will check replicas for', connectString
    else:
        print 'ERROR! You must set the environment variable %s'%(envKey)
        print 'ERROR! Optionally you can set also %s'%(envKeyR)
        sys.exit(-1)

    # Debug printout for bug #32362 (schema evolution test on Win nightlies)
    ###print 'DEBUG: PYTHONPATH =', os.environ["PYTHONPATH"]

    # Debug printout for bug #45732 (schema evolution tests use wrong C++?)
    ###if "LD_LIBRARY_PATH" in os.environ:
    ###    print 'DEBUG: LD_LIBRARY_PATH =', os.environ["LD_LIBRARY_PATH"]

    # Disable replica failover in CORAL to prevent silent test failures against
    # CoralServer or Frontier when these are not available, otherwise the R/O
    # test is executed succesfully against Oracle (CORALCOOL-2120). Disabling
    # failover is a better fix than the older patch using CoolAuthentication,
    # which was broken if COOLTESTDB_R is defined (CORALCOOL-2917).
    app = cool.Application()
    app.connectionSvc().configuration().disableReplicaFailOver()

    # No need to check the R/W connection string (see below), use it as-is.
    if 'ReferenceDbMgr' in dir():
        ###print 'DEBUG: Instantiate a', refSchemaVersion, 'ref db manager'
        mgr = ReferenceDbMgr( refSchemaVersion )
        ###print 'DEBUG: Drop and recreate reference database'
        ###print 'DEBUG: -> db ID:', connectString
        mgr.dropReferenceDb( connectString )
        mgr.createReferenceDb( connectString )
        # Stop here if there is nothing else to do
        if 'TestReferenceDb1RO' not in dir() and 'TestReferenceDb1RW' not in dir() and 'TestReferenceDb2RO' not in dir():
            sys.exit(0)
    elif 'TestReferenceDb1RO' not in dir() and 'TestReferenceDb1RW' not in dir() and 'TestReferenceDb2RO' not in dir():
        print "WARNING! No module was loaded, there is nothing to do"
        sys.exit(0)        

    # No need to check the R/W connection string. Check only the R/O connection
    # string: if it is a Frontier or CoralServer connection, then grant Oracle
    # reader privileges to public; in addition refresh the server cache if this
    # is a Frontier server cache connection (i.e. port 8080, not port 8000).
    import CoolAuthentication
    propRO = CoolAuthentication.getDbIdProperties( connectStringR, False )
    ###print 'DEBUG: Properties of first replica (R/O test):', propRO # WARNING! THIS DUMPS PASSWORDS IN CLEAR TEXT!
    replRO = CoolAuthentication.coralReplica( propRO )
    print 'DEBUG: Looked up first replica (R/O test):', replRO

    # Build the URL required by IWebCacheControl::refreshSchemaInfo
    # - Determine the explicit frontier replica, and then either of:
    # - Strip off the trailing schema
    # - 1. 'frontier://(serverurl=)()/schema' => '(serverurl=)()'
    # - 2. 'frontier://h:p/servlet/schema' => 'http://h:p/servlet'
    if replRO.find('frontier') == 0 and replRO.find(':8000/') < 0:
        print 'DEBUG: Frontier server replica: no cache to refresh'
    elif replRO.find('frontier') == 0:
        print 'DEBUG: Frontier cache replica: configure refresh'
        webCacheUrl=replRO
        # Strip off schema
        if webCacheUrl.rfind('/') < 0:
            print 'ERROR! Cannot strip schema off', webCacheUrl
            sys.exit(-1)
        webCacheUrl=webCacheUrl[:webCacheUrl.rfind('/')]
        if webCacheUrl.find('frontier://(') == 0:
            # 1: 'frontier://(serverurl=http://h:p/servlet)()/schema'
            webCacheUrl=webCacheUrl[webCacheUrl.find('('):]
        else:
            # 2: 'frontier://host:port/servlet/schema'
            webCacheUrl=webCacheUrl.replace('frontier','http',1)
        print 'DEBUG: Will refresh Frontier cache for', webCacheUrl
        TestReferenceDb1RO.invalidateCacheUrl=webCacheUrl
        if 'TestReferenceDb2RO' in dir():
            TestReferenceDb2RO.invalidateCacheUrl=webCacheUrl
    else:
        print 'DEBUG: Not a Frontier replica: no cache to refresh'

    # If R/O replica is a Frontier or CoralServer connection,
    # then grant Oracle reader privileges to public.
    if replRO.find('frontier') == 0 or replRO.find('coral') == 0:
        print 'DEBUG: Grant READER privs to public',\
              '(for Frontier and CoralServer R/O tests)'
        cmd = 'coolPrivileges "' # You may need to add .exe on Windows...
        cmd = cmd + connectString + '" GRANT READER public'
        print 'DEBUG: Execute:', cmd
        if platform.system() == 'Darwin':
            # Workaround for SIP on MacOSX (CORALCOOL-2884)
            # Use own copy of /bin/bash instead of defult /bin/sh
            f = Popen( cmd, stdout=PIPE, stderr=STDOUT, shell=True, executable="bash" )
        else:
            f = Popen( cmd, stdout=PIPE, stderr=STDOUT, shell=True )
        line = f.stdout.readline()
        while line :
            if line.find("ERROR") == 0: print line.strip()
            line = f.stdout.readline()
        status = f.wait()
        if status : # Check coolPrivileges status (fix bug #83515)
            print "ERROR! Command '" + cmd + "' failed! Status:", status
            sys.exit(-1)
        if 'TestReferenceDb1RW' in dir():
            TestReferenceDb1RW.grantPublicReader=True

    if 'TestReferenceDb1RO' in dir():
        print 'DEBUG: Execute 1RO tests against reference database'
        print 'DEBUG: -> db ID:', connectStringR
        TestReferenceDb1RO.connectString = connectStringR
        TestReferenceDb1RO.refSchemaVersion = refSchemaVersion

    if 'TestReferenceDb1RW' in dir():
        print 'DEBUG: Execute 1RW tests against reference database'
        print 'DEBUG: -> db ID:', connectString
        TestReferenceDb1RW.connectString = connectString
        TestReferenceDb1RW.refSchemaVersion = refSchemaVersion

    if 'TestReferenceDb2RO' in dir():
        print 'DEBUG: Execute 2RO tests against reference database'
        print 'DEBUG: -> db ID:', connectStringR
        TestReferenceDb2RO.connectString = connectStringR
        TestReferenceDb2RO.refSchemaVersion = refSchemaVersion

    ###print 'DEBUG: Import unittest'
    import unittest

    ###print 'DEBUG: Run the unit tests'
    unittest.main( testRunner =
                   unittest.TextTestRunner(stream=sys.stdout,verbosity=2) )
