Package PyCoolUtilities
Package manager: Sven A. Schmidt and Marco Clemencic
  
==============================================================================
!2008.11.10 - Andrea

Internal doc for tag COOL_2_6_0.

No changes in PyCoolUtilities with respect to COOL_2_6_0-pre6.

==============================================================================
!2008.10.21 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_2_4_0b. Rebuild of the 2.4.0a release for the LCG_54h configuration.
Add support for Oracle on MacOSX. Bug fixes in CORAL and frontier_client.
No change in the source code. Software version remains "2.4.0".

NB: None of the _code_ changes in COOL_2_4-branch are released in COOL_2_4_0b!
NB: [there is one minor exception, RelationalCool/tests/RelationalDatabaseId]
NB: Only the _config_ branch changes are released in COOL_2_4_0b!

==============================================================================
!2008.10.16 - Andrea

Internal tag COOL_2_6_0-pre5-bis ('2008/10/16 10:00:00').

Changes in PyCoolUtilities with respect to COOL_2_6_0-pre5:
- Improve the COOL configuration for Frontier Squid cache tests (task #3440).
  Fix bug #42354 (refresh the cache also at the beginning of the 1RO test).
  Add a test for Frontier bug #42465 (check persistent updates of the squid).
- Improve the regression and schema evolution tests.
  Replace testReferenceDb(RW/RO).py by three separate scripts 
  testReference(Db1RO|Db1RW|Db2RO).py matching PyCoolReferenceDB.
  Add a fourth script testReferenceDbAll.py that performs 
  all three tests for the QMTEST schema evolution test.
- Add a test for lowercase payload in the regression tests.
- Add gcc43 to all .cvsignore files.

==============================================================================
!2008.09.27 - Andrea

Internal tag COOL_2_6_0-pre5 (-D '2008/09/27 10:00:00').

No changes in PyCoolUtilities with respect to COOL_2_6_0-pre4.

==============================================================================
!2008.09.25 - Andrea

Internal tag COOL_2_6_0-pre4.

No changes in PyCoolUtilities with respect to COOL_2_6_0-pre3.

==============================================================================
!2008.09.11 - Andrea

Internal tag COOL_2_6_0-pre3.

No changes in PyCoolUtilities with respect to COOL_2_6_0-pre2.

==============================================================================
!2008.09.01 - Andrea

Internal tag COOL_2_6_0-pre2.

Summary of changes in PyCoolUtilities with respect to COOL_2_6_0-pre1:
- Extend and reenable test for setTagDescription in replication (task #6541).
- Add infrastructure for tests of the Frontier Squid cache (task #3440).
  > Reorganize the regression tests so that a second R/O test using Frontier 
    is executed after the Oracle R/W test that updates the (cacheable) data.
  > Open dbs in test_00 and close dbs in test_99 in PyCoolReferenceDb.
  > Invalidate the Frontier Squid cache using IWebCacheControl
    before the execution of the second R/O test in the regression tests.
  > Improve the regression test for HVS tag relations.

==============================================================================
!2008.08.27 - Andrea

Internal tag COOL_2_6_0-pre1.

Summary of changes in PyCoolUtilities with respect to COOL_2_5_0:
- Upgrade COOL software version to 2.6.0.
- Enhancements in coolAuthentication and the regression test configuration.
  > Use a single Frontier alias for R/O and R/W tests (task #3442).
  > Adapt the PyCoolReferenceDb package and the regression test
    to the new coolAuthentication (which displays all CORAL replicas).
- Add cmt/version.cmt to prepare support for the latest CMT version.
- Remove the old test infrastructure using bash scripts.
- Remove a debug printout in the replication test.

==============================================================================
!2008.07.16 - Andrea

Add workaround for LCGCMT bug #38934 (python module installation is broken 
by LCGCMT_55a). This was removed immediately after the LCGCMT bug fix.

[Changes included by Andrea in COOL_2_4-branch on 2008.07.16]

==============================================================================
!2008.06.10 - Andrea

Tag COOL_2_5_0. Production release with API semantic changes, API extensions
and package structure changes to remove the dependency on SEAL.
Upgrade to LCG_55 using the 'de-SEALed' CORAL_2_0_0.

Changes in PyCoolUtilities with respect to COOL_2_4_0a:
- Add support for software version 2.5.0 in the reference database tool.

==============================================================================
!2008.06.04 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_2_4_0a. Rebuild of the 2.4.0 release for the LCG_54g configuration.
Port to osx105 and .so shared lib names on MacOSX. Changes in CORAL and ROOT.
No change in the source code. Software version remains "2.4.0".

NB: None of the _code_ changes in COOL_2_4-branch are released in COOL_2_4_0a!
NB: Only the (osx105) _config_ branch changes are released in COOL_2_4_0a!

==============================================================================
!2008.02.28 - Andrea

Tag COOL_2_4_0. Production release with backward compatible API extensions.
Upgrade to LCG_54b using CORAL_1_9_5.

==============================================================================
!2008.02.25 - Andrea

Add a live iterator test for all backends. Thinking of the CORAL server...

==============================================================================
!2008.02.19 - Andrea (changes committed before COOL_2_3_1 but not tagged)

Upgrade software version to 2.4.0.

Add a test (commented out) for setTagDescription (task #6394) in the 
replication tool (check that tag description changes are replicated).

==============================================================================
!2008.02.21 - Andrea

Tag COOL_2_3_1. Bug-fix production release (binary compatible with 2.3.0) 
with performance optimizations for multi-version retrieval and insertion.
Upgrade to LCG_54a including bug fixes in ROOT 5.18.00a. 

==============================================================================
!2008.02.13 - Andrea (-D '2008/02/13 19:30:00')

Internal tag COOL_2_3_1-pre2 (bug fixes and performance optimizations).
Use the default LCG_54 (including ROOT 5.18) on all platforms.

==============================================================================
!2008.02.04 - Andrea

Add printouts for bug #33399 in the replication test.

==============================================================================
!2008.02.04 - Andrea

Internal tag COOL_2_3_1-pre1 (bug fixes and performance optimizations).
Upgrade COOL software version to 2.3.1.

==============================================================================
!2008.02.04 - Sven

Improved test for bug #32976 in the replication tool.

==============================================================================
!2008.01.29 - Marco

Added a test for bug #32976 in the replication tool.

==============================================================================
!2008.01.21 - Andrea

Tag COOL_2_3_0. Production release with backward compatible API extensions.
Upgrade to LCG_54 using Python 2.5, ROOT 5.18 and several other new externals.

PyCool is not supported on MacOSX because of bug #32770 in ROOT 5.18.00.
The COOL nightlies are all green (except for the PyCool tests on MacOSX).

==============================================================================
!2008.01.18 - Andrea

Add a debug printout for bug #32362 (schema evolution test on Win nightlies).

==============================================================================
!2007.12.19 - Andrea

Internal tag COOL_2_3_0-pre1 (new COOL API extensions on standard LCG_53f).
All CMT tests successful, bash tests not updated. Software version is 2.3.0.

==============================================================================
!2007.12.14 - Andrea

Upgrade COOL software version to 2.3.0.

==============================================================================
!2007.10.13 - Andrea

Tag COOL_2_2_2. Production release (binary compatible with 2.2.0) with many
performance and configuration improvements and bug fixes. New versions of 
CORAL and Frontier server (fixing all pending problems in the tests) and ROOT.
This is the first COOL release built by the SPI team (and no SCRAM config).

==============================================================================
!2007.11.08 - Andrea

Internal tag COOL_2_2_2-pre2 (using private CORAL192 build and SEAL193 copy).
All CMT tests successful, bash tests not updated. Software version is 2.2.2.

==============================================================================
!2007.11.07 - Andrea

Internal tag COOL_2_2_2-pre1 (using private CORAL192 build and SEAL193 copy).
All CMT tests successful (no pending Frontier failures).
Bash tests not updated (Wine failures are expected).

==============================================================================
!2007.10.30 - Andrea

Added a fake make target "examples" to please nmake (task #5414).

==============================================================================
!2007.10.24 - Andrea

Add the addIovIntoNewChannel.py test to add new IOVs into new channels.
This is used in test_SchemaEvolutionFrom200.sh, together with the new
coolValidateSchema tool, as part of cross-checks for bug #23755.
 
==============================================================================
!2007.10.23 - Andrea

Fix bash test scripts for future schema evolution tests on MacOSX (these tests 
are not executed in COOL_2_2_1 as no previous release is supported on MacOSX).

==============================================================================
!2007.10.20 - Sven

Added two tests for bug #30578 in the replication tool.

==============================================================================
!2007.10.16 - Andrea

Remove obsolete output file from mysql regression test.

==============================================================================
!2007.10.16 - Andrea

Add tests for bug #30431 (MV existsChannel) and bug #30443 (MV listChannels) 
to the regression test suite. These produce the following failures:

- In the regression test and test_SchemaEvolutionFrom200: 
  FAIL: test_RO_03b_listChannels_MV (PyCoolReferenceDb.TestReferenceDbRO)
    self.assertEquals( channels.size(), 4 )
  AssertionError: 2L != 4

- In the regression test and test_SchemaEvolutionFrom200: 
  FAIL: test_RO_15_channelsExist_MV (PyCoolReferenceDb.TestReferenceDbRO)
    self.assertEquals( fmv1.existsChannel( 10 ), True )
  AssertionError: 0 != True

In test_SchemaEvolutionFrom200 the failures only take place in the readback
using the latest COOL (those that would happen using COOL_2_1_1 are disabled).

==============================================================================
!2007.10.11 - Andrea

Tag COOL_2_2_1. Production release (binary compatible with 2.2.0) with many
configuration improvements, feature enhancements and bug fixes. New versions 
of CORAL (with important bug fixes for SQLite and Frontier), ROOT and SEAL.
This is the first COOL release with support for MacOSX/Intel platforms.

==============================================================================
!2007.10.08 - Andrea

Internal tag COOL_2_2_1-pre5. Last private tag before COOL_2_2_1.
All CMT/SCRAM tests successful on all platforms using a private CORAL191
(except for the last pending failure RO_02b1 - bug #23368 in FrontierAccess),
including MacOSX/PPC (last build) and gcc41 (not built for official COOL221).
Non-debug osx104_ia32_gcc401 not built, test results copied from debug version.

Changes in PyCoolUtilities code, tests and test results (none in config):
- Test RO_02b1 for sqlite blobs succeeds (fixed bug #30036, aka bug #22485)
  in both the regression and schema evolution tests.
  > Schema evolution tests had to be fixed to skip the comparison of sqlite
    data written with older versions of CORAL affected by bug #30036.
- Test RO_06b for frontier uchar succeeds (fixed bug #29370 and bug #29266).
- Test RO_02, RO_02a and RO_02b for frontier long succeeds (fixed bug #23369, 
  thanks to fixes for bug #29266 and #29187 reported by CMS.
- Software version is now 2.2.1
- Update test results for cygwin (.NT) again..

==============================================================================
!2007.08.29 - Andrea (-D '2007/08/29 13:30:00')

Internal tag COOL_2_2_1-pre4.
First version with successful tests of slc4_ia32_gcc41 on SCRAM/BASH,
including SQLite and Frontier support and complete private config for CMT.

No changes in PyCoolUtilities code.

Changes in bash test results:
- All OK for slc4_ia32_gcc41 (after enabling SQLite/Frontier).
- Replace 'Skip test' by 'Skip' in existing logfiles for mergeTestOutput.

Changes in test config:
- Replace 'Skip test' by 'Skip' for mergeTestOutput.

==============================================================================
!2007.08.24 - Andrea (-D '2007/08/24 17:00:00')

Internal tag COOL_2_2_1-pre3.
First version with successful tests of MacOSX Intel on both CMT/QMTEST 
and SCRAM/BASH, including PyCool (but still no Oracle support).
First version with successful tests of slc4_ia32_gcc41 on SCRAM/BASH
(but CMT is badly configured, and still no SQLite or Frontier support).

No changes in PyCoolUtilities code.

Changes in SCRAM config:
- Workaround for ROOT bug #22003 on MacOSX (task #5449): '.dylib' is not one 
  of the suffixes considered by ROOT when looking for the dictionary library.
  Create a symlink from 'dylib' to 'so' for SCRAM (when running the tests).

Changes in test config:
- Echo a message when a test is skipped in the BASH test script.
- Skip all BASH tests for MacOSX PowerPC (PyROOT not supported).
- Skip PyCoolConsole test on MacOSX Intel (readline not supported, sr #102398).

Changes in BASH test results:
- All PyCool tests are now successful on MacOSX Intel.
- Added logfile for slc4_ia32_gcc41 (all tests OK using SCRAM).
- New tests for bug #28189 and bug #28787 are OK.
- Echo messages that all tests for PowerPC are skipped (PyROOT not supported).

==============================================================================
!2007.08.07 - Andrea (-D '2007/08/07 16:02:00')

Internal tag COOL_2_2_1-pre2. 
First version with all QMTEST tests as successful as bash tests.

No changes in PyCoolUtilities code, config or bash test results.

==============================================================================
!2007.08.02 - Andrea (-D '2007/08/03 14:05:00')

Internal tag COOL_2_2_1-pre1. 

Regression test improvements:
- Reenable CoolQueryManager for Oracle on Windows (previously disabled
  to fix bug #28109) after completing its port (task #5255).

No changes in bash test results.

==============================================================================
!2007.07.13 - Andrea

Tag COOL_2_2_0. Production release with many performance optimizations and bug 
fixes, also including backward-compatible API extensions and schema changes.
New versions of CMT, CORAL, ROOT/Reflex, oracle, sqlite, frontier_client
and LFC using LCG_53 (with respect to COOL 2.1.1 using LCG_51).

Main changes in PyCoolUtilities:
- Added support for Blob16M (task #2197).
- Improved test infrastructure to create/evolve/test reference databases
  created using either the COOL_1_3_4 or the COOL_2_1_1 release.
- Fix bug #28109 in configuration of Oracle schema evolution tests on Windows.
- Fix bug #28075 in CMT configuration of schema evolution tests.
- Fix bug #28040 in configuration of regression tests for frontier.
- Fix bug #28039 in configuration of schema evolution tests on AMD64.

==============================================================================
!2007.04.16 - Marco

Tag COOL_2_1_1. Bug-fix production release (binary compatible with 2.1.0).
New versions of CORAL, ROOT and frontier_client using LCGCMT_51.

==============================================================================
!2007.03.24 - Andrea

Tag COOL_2_1_0. Production release with backward-compatible API extensions 
and schema changes. New versions of CORAL and ROOT using LCGCMT_50c.

==============================================================================
!2007.02.14 - Marco

Added the tool PyCoolDiff to extract the difference between to DBs.
The difference between two DBs is defined as the set of IOVs extracted from
the second DB that applied to the first one will make it look like the second.
(Test are missing)

Updated the html documentation of the utils.

==============================================================================
!2007.02.13 - Marco

Fixed bug #23716.

==============================================================================
!2007.01.31 - Andrea

Tag COOL_2_0_0. Major production release with backward-incompatible
API and schema changes. New versions of SEAL, CORAL and frontier_client. 

Improved regression tests in the PyCoolUtilities test suite.
Included schema evolution tests in the PyCoolUtilities test suite.

==============================================================================
!2007.01.17 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_4. Production release (backward-compatible bug-fix, 
functionality enhancement and LCG_50 configuration upgrade release). 
Important fixes in SEAL (component model and multithreading), CORAL and ROOT.

==============================================================================
!2006.12.21 - Andrea

Completed schema evolution tests.

Schema evolution tests are disabled on AMD64 because it is not possible to
create a 1.3.0 schema using the COOL133c release on AMD64 (long vs long long).

==============================================================================
!2006.12.12 - Marco

Updated to use Record instead of AttributeList.

==============================================================================
!2006.11.26 - Andrea

Moved ReferenceDb tool from RelationalCool tests to PyCoolUtilities as 
new package PyCoolReferenceDb (this is a python package based on PyCool).
This ensures that the python package is compiled in the platform-dependent 
area, avoiding interference between 32 and 64bit Linux (fix for bug #21391).

Moved execution of the regression tests based on ReferenceDb tool from 
PyCool to PyCoolUtilities. This includes the Frontier regression test.

[Changes included by Andrea in COOL_1_3-branch on 2006.11.26]

==============================================================================
!2006.11.25 - Andrea

Reenabled MySQL tests on Windows.

==============================================================================
!2006.10.30 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3c. Rebuild of the 1.3.3 release for the LCG_48 configuration.
Important fixes in SEAL (component model and multithreading), CORAL and ROOT.
No change in the source code. Software version remains "1.3.3".

==============================================================================
!2006.10.16 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3b. Rebuild of the 1.3.3 release for the LCG_47b configuration.
New version of ROOT. Pending bugs in SEAL and ROOT. 
Partial support for MySQL on Windows (pending PyCoolUtilities bug #20780). 
No change in the source code. Software version remains "1.3.3".

===============================================================================
!2006.10.06 - Marco

Adapted to the changes in the API done by Andrea.

Applied Nicolas' patch for bug #19969.

===============================================================================
!2006.09.29 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3a. Rebuild release (bug fixes in CORAL and frontier_client).
Pending bugs in SEAL, new bugs in ROOT. Same source code as COOL_1_3_3. 
Only added two Frontier regression tests. Software version remains "1.3.3".

==============================================================================
!2006.08.28 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3. Production release (backward-compatible bug-fix, 
functionality enhancement and configuration upgrade release).
Many important fixes in CORAL and Frontier; pending critical bugs in SEAL.

==============================================================================
!2006.07.26 - Marco for Nicolas Gilardi

 - Upgraded PyCoolCopy module to work with HVS from COOL 1.3. Changes are
   backward compatible.

 - Created PyCoolCopy.restoreTagRelation() and Selections.extendTagList().

 - Major modifications in PyCoolCopy.copy(), PyCoolCopy.copyFolder to give HVS
   compatibility.

 - Created the TestHVSCopy class to test the copy of tag hierarchy

 - New features related to HVS compatibility:
    * Selections objects can contain tags owned by foldersets.
    * Selections objects must contain tags that are ancestors of, or owned by
      the selected node.
    * When a node is copied for a given tag, all the relations between this tag
      and its child tags are also copied to the target database.
      WARNING: relations between the given tag and its ancestors are not copied

      Example:

      We have the database:
      Node Name    Type         tag Names and parents
      /            FolderSet    v1; v2
      /a           FolderSet    a-v1 -> v1; a-v2 -> v2
      /a/b         FolderSet    b-v1 -> a-v1; b-v2 -> a-v2
      /a/bb        Folder       bb-data -> a-v2
      /a/b/c       Folder       c-data -> b-v1; c-data -> b-v2

      If our selection object is:

      s = ['/a/b', 0, inf, all, ['v1']]

      the target database will contain:
      Node Name    Type         tag Names and parents
      /            FolderSet    v1
      /a           FolderSet    a-v1 -> v1
      /a/b         FolderSet    b-v1 -> a-v1
      /a/b/c       Folder       c-data -> b-v1

      If our selection object is:

      s = ['/a/b', 0, inf, all, ['b-v2']]

      the target database will contain:
      Node Name    Type         tag Names and parents
      /            FolderSet
      /a           FolderSet
      /a/b         FolderSet    b-v2
      /a/b/c       Folder       c-data -> b-v2

 - Created PyCoolCopy.append() function. It allows to copy selection objects
   to the HEAD of an existing database. Only one tag is allowed per selection
   object to append and it is not reproduced in the target database.

 - Created append(), a function using PyCoolCopy.append(), with only one
   selection object and without explicitely creating a PyCoolCopy object
   (similar to the copy() function).

 - Created TestAppend class to test the append() function.

[Changes included by Marco in COOL_1_3-branch on 2006.07.26]

==============================================================================
!2006.07.25 - Andrea

Minor fixes to the test scripts for removing sqlite files on wine.

[Changes included by Andrea in COOL_1_3-branch on 2006.07.25]

==============================================================================
!2006.07.12 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_2c. Rebuild of COOL_1_3_2 for the LCG_46 configuration.
No change in the source code. Software version remains "1.3.2".

Andrea: include in COOL_1_3_2c retag the small fix in execUnitTests.sh
to use cmd_wrapper on wine [included in COOL HEAD on 2006.05.18].

==============================================================================
!2006.06.19 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_2b. Rebuild of COOL_1_3_2 for the LCG_45 configuration.
No change in the source code. Software version remains "1.3.2".

==============================================================================
!2006.05.18 - Andrea

Minor fix in execUnitTests.sh to use cmd_wrapper on wine.

[Changes included by Andrea in COOL_1_3-branch on 2006.07.25]

==============================================================================
!2006.05.14 - Andrea

Tag COOL_1_3_2a. Rebuild of the 1.3.2 release for the LCG_44 configuration.

==============================================================================
!2006.05.10 - Andrea

Tag COOL_1_3_2. Production release (backward-compatible 
bug-fix and Frontier support release in the 1.3 series).

==============================================================================
!2006.04.25 - Andrea, Marco, Sven

Tag COOL_1_3_1. Production release (backward-compatible 
COOL and CORAL bug-fix release in the 1.3 series).

==============================================================================
!2006.04.25 - Marco

Fixed a "feature" in PyCoolTool that was preventing to use aliases.

==============================================================================
!2006.04.06 - Andrea

Tag COOL_1_3_0. Functionality enhancement production release (first 
release in the 1.3 series: backward incompatible API and schema changes).

First production release including the new PyCool implementation.

==============================================================================
!2006.04.06 - Andrea

Problems for Windows:
- unknown exception thrown (workaround by Andrea)
- os.sysconf not defined (fixed by Marco)
- prevent MySQL execution of coolDropDB (fixed by Andrea)
- ROOT window still pops up (pending)

==============================================================================
!2006.03.08 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_2_9 (non-HEAD branch after COOL_1_2_8).
Backward-compatible production release.
Same code as 1.2.8, but compiled against LCG_42_4.

==============================================================================
!2006.01.27 - Andrea

Tag COOL_1_2_8. Backward-compatible production release (internal migration 
from SEAL Reflex to ROOT Reflex; port to gcc344; attempted port to AMD64).

==============================================================================
!2006.01.16 - Andrea

Tag COOL_1_2_7. Backward-compatible production release
(internal migration from RAL to CORAL and from Reflection to seal Reflex).

==============================================================================
!2006.01.11 - Marco

Complete migration of PyCool from seal Reflection to seal Reflex.
New mutex lock problem observed from boost/PyROOT/Reflex interaction.

==============================================================================
!2005.11.15 - Marco

Tag COOL_1_2_6. Production release (backward-compatible 
SEAL_1_7_6 and POOL_2_2_4 upgrade release in the 1.2 series).

==============================================================================
!2006.11.15 - Andrea

This package contains python utilities for COOL
(copy tool, inspection tool, console tool).

Documentation is maintained by Sven in html format.

==============================================================================
