# See also test_root6068b.py (and CORALCOOL-2959)
import sys
if len(sys.argv)!=2 or ( sys.argv[1]!='nocrash' and sys.argv[1]!='crash1' and sys.argv[1]!='crash2' ):
    # NB: 'crash1' and 'crash2' cause a crash, 'nocrash' does not...
    print "Usage: python", sys.argv[0], "<nocrash|crash1|crash2>"
    sys.exit(0)
crash1=False
crash2=False
if sys.argv[1]=='crash1': crash1=True
elif sys.argv[1]=='crash2': crash2=True
print "Your choice: CRASH1 (do not add a printout) =",crash1
print "Your choice: CRASH2 (do not add a template) =",crash2

# Import mini-PyCool
import os
dir=os.path.dirname( os.path.realpath(__file__) )
import cppyy
###cppyy.gbl.gSystem.Load('liblcg_CoolApplication.so')
cppyy.gbl.gSystem.Load('liblcg_CoolKernel.so')
if crash1: cppyy.gbl.gInterpreter.ProcessLine("#define CRASH1 1")
if crash2: cppyy.gbl.gInterpreter.ProcessLine("#define CRASH2 1")
cppyy.gbl.gInterpreter.ProcessLine('#include "'+dir+'/test_root8458f.h"')
cool  = cppyy.gbl.cool

# Test (as in test_root6068.py, unstable vs crash/nocrash)
cool.FieldSelection4("i",cool.StorageType.Int32,cool.FieldSelection4.EQ,10)

# Test (as in the original test_root8458.py, no instability observed)
#cool.FieldSelection4("x",cool.StorageType.Bool,cool.FieldSelection4.EQ,True)
#cool.FieldSelection4("x",cool.StorageType.Bool,cool.FieldSelection4.EQ,10)
#cool.FieldSelection4("x",cool.StorageType.Bool,cool.FieldSelection4.EQ,10.)
