# See also test_root6068.py
import ROOT
ROOT.gInterpreter.ProcessLine("""
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <typeinfo>
namespace cool {
  namespace StorageType { 
    enum TypeId { Bool=0, Int32=1, UInt32=2, Float=3, Double=4 };
    const std::type_info& cppType( TypeId id )
    {
      switch ( id ) {
      case StorageType::TypeId::Bool:    return typeid( bool );
      case StorageType::TypeId::Int32:   return typeid( int );
      case StorageType::TypeId::UInt32:  return typeid( unsigned int );
      case StorageType::TypeId::Float:   return typeid( float );
      case StorageType::TypeId::Double:  return typeid( double );
      }
    }
  }
  std::map<StorageType::TypeId,std::string> typeIdName {
    {StorageType::Bool,\"Bool\"},
    {StorageType::Int32,\"Int32\"},
    {StorageType::UInt32,\"UInt32\"},
    {StorageType::Float,\"Float\"},
    {StorageType::Double,\"Double\"}
  };
  struct FieldSelection {
    enum Relation { EQ, NE, GT, GE, LT, LE };
    template<typename T> FieldSelection( const std::string& /*name*/,
                                         const StorageType::TypeId typeId,
                                         Relation /*relation*/,
                                         const T& refValue )
    {
      std::cout << \"FieldSelection for StorageType=\" << typeId << \"(\" << typeIdName[typeId] << \",\" << StorageType::cppType(typeId).name() << \") against (\" << typeid(T).name() << \")=\" << refValue << std::endl;
      if ( typeid(T) != StorageType::cppType(typeId) )
        throw std::runtime_error( \"Type mismatch between \" + typeIdName[typeId] + \" and \"+std::string(typeid(T).name()) );
    }
  };
  template FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const bool& refValue );
  template FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const int& refValue );
  template FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const float& refValue );
}
""")

cool = ROOT.cool
for typeId in cool.StorageType.Bool, cool.StorageType.Int32, cool.StorageType.UInt32, cool.StorageType.Float: #, cool.StorageType.Double:
    print "================================"
    typeName=cool.typeIdName[typeId]
    print typeId, typeName
    for refValue in True, 10, 10.1: #, "string":
        try:
            print "TRY FS ctor for "+typeName+" against refValue="+str(refValue)
            fs = cool.FieldSelection("x",typeId,cool.FieldSelection.EQ,refValue)
            print "OK!"
        ###except Exception,e: print "FAILED!", e
        except: print "FAILED!"
        print
