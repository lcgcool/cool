#include "test_root8458b.h"
namespace cool {
  namespace StorageType { 
    void throwIfMismatch( const std::type_info& typeIdT, TypeId typeId )
    {
      if ( typeIdT != StorageType::cppType(typeId) )
        throw std::runtime_error( "Type mismatch between " + typeIdName[typeId] + " and "+std::string(typeIdT.name()) );
    }
  }
}
#ifdef MAIN
int main()
{
  return 0;
}
#endif
