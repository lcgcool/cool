#ifndef TEST_ROOT8458G_LIB_H
#define TEST_ROOT8458G_LIB_H 1

// Include files
#include <climits>
#include <string>
#include <memory>
#include <typeinfo>
#include <vector>

namespace rbug
{

  // Forward declarations
  struct Attribute4;
  struct IRecordSpecification4;

  //---------------------------------------------------------------------------

  // Dummy structs
  struct IRecord4{};
  struct AttributeList4{};
  struct Blob4{};

  //---------------------------------------------------------------------------

  typedef bool Bool;
  typedef signed char SChar;
  typedef unsigned char UChar;
  typedef short Int16;
  typedef unsigned short UInt16;
  typedef int Int32;
  typedef unsigned int UInt32;
  typedef long long Int64;
  typedef unsigned long long UInt64;
  typedef UInt64 UInt63;
  typedef float Float;
  typedef double Double;
  typedef std::string String255;
  typedef std::string String4k;
  typedef std::string String64k;
  typedef std::string String16M;
  typedef std::string String128M;
  typedef Blob4 Blob64k;
  typedef Blob4 Blob16M;
  typedef Blob4 Blob128M;

  const SChar SCharMin = SCHAR_MIN; // -127
  const SChar SCharMax = SCHAR_MAX; // +128
  const UChar UCharMin = 0;
  const UChar UCharMax = UCHAR_MAX; // +255
  const Int16 Int16Min  = SHRT_MIN; // -32768
  const Int16 Int16Max  = SHRT_MAX; // +32767
  const UInt16 UInt16Min = 0;
  const UInt16 UInt16Max = USHRT_MAX; // +65535
  const Int32 Int32Min  = INT_MIN; // -2147483648
  const Int32 Int32Max  = INT_MAX; // +2147483647
  const UInt32 UInt32Min = 0;
  const UInt32 UInt32Max = UINT_MAX; // +4294967295
  const Int64 Int64Min  = LONG_LONG_MIN; // -9223372036854775808
  const Int64 Int64Max  = LONG_LONG_MAX; // +9223372036854775807
  const UInt64 UInt64Min = 0;
  const UInt64 UInt64Max = ULONG_LONG_MAX; // +18446744073709551615
  const UInt63 UInt63Min = UInt64Min;
  const UInt63 UInt63Max = Int64Max;

  // -------------------------------------------------------------------------

  class StorageType4
  {
  public:
    enum TypeId { Bool, UChar, Int16, UInt16, Int32, UInt32, UInt63, Int64,
                  Float, Double, String255, String4k, String64k, String16M,
                  String128M, Blob64k, Blob16M, Blob128M };
  public:
    virtual ~StorageType4() {}
    static const StorageType4& storageType( const TypeId& id );
    const TypeId& id() const{ return m_id; }
    const std::string name() const;
    const std::type_info& cppType() const;
    size_t maxSize() const;
    bool operator== ( const StorageType4& rhs ) const{ return rhs.id() == id(); }
    bool operator!= ( const StorageType4& rhs ) const{ return rhs.id() != id(); }
    bool operator== ( const TypeId& rhs ) const{ return rhs == id(); }
    bool operator!= ( const TypeId& rhs ) const{ return rhs != id(); }
    template <typename T> void validate( const T& data, const std::string& variableName="" ) const {
      validate( typeid( T ), (const void*)&data, variableName );
    }
  private:
    StorageType4( const TypeId& id ) : m_id( id ) {}
    StorageType4();
    StorageType4( const StorageType4& rhs );
    StorageType4& operator=( const StorageType4& rhs );
    void validate( const std::type_info& cppTypeOfData, const void* addressOfData, const std::string& variableName ) const;
  private:
    const TypeId m_id;
  };

  // -------------------------------------------------------------------------

  struct IFieldSpecification4{
    virtual ~IFieldSpecification4() {}
    virtual const std::string& name() const = 0;
    virtual const StorageType4& storageType() const = 0;
  };

  //---------------------------------------------------------------------------

  struct IField4{
    virtual ~IField4() {}
    virtual const IFieldSpecification4& specification() const = 0;
    const std::string& name() const{ return specification().name(); }
    const StorageType4& storageType() const{ return specification().storageType(); }
    template<typename T> void setValue( const T& value ){ this->setValue( typeid(T), &value ); }
    virtual void setValue( const std::type_info& cppType, const void* externalAddress ) = 0;
  };

  //---------------------------------------------------------------------------

  class FieldSpecification4 : public IFieldSpecification4
  {
  public:
    virtual ~FieldSpecification4();
    FieldSpecification4( const std::string& nm, const StorageType4::TypeId id );
  private:
    FieldSpecification4( const FieldSpecification4& rhs );
  public:
    const std::string& name() const;
    const StorageType4& storageType() const;
    bool operator==( const IFieldSpecification4& rhs ) const;
    bool operator!=( const IFieldSpecification4& rhs ) const;
    void validate( const IField4& field, bool checkName = true ) const;
    void validate( const Attribute4& at, bool checkName = true ) const;
  private:
    FieldSpecification4();
    FieldSpecification4& operator=( const FieldSpecification4& rhs );
  private:
    const std::string m_name;
    const StorageType4::TypeId m_typeId;
  };

  //---------------------------------------------------------------------------

  struct RecordSpecification4{ 
    RecordSpecification4() : m_pfspec() {}
    RecordSpecification4( FieldSpecification4* pfspec ) : m_pfspec( pfspec ){}
    std::unique_ptr<FieldSpecification4> m_pfspec; 
  };
  
  //---------------------------------------------------------------------------

  class Record4 : public IRecord4
  {
  public:
    virtual ~Record4();
    Record4();
    Record4( const IRecordSpecification4& sp );
    Record4( const IFieldSpecification4& sp );
    Record4( const IRecordSpecification4& sp, const AttributeList4& al );
    Record4( const Record4& rhs );
    Record4( const IRecord4& rhs );
    Record4& operator=( const Record4& rhs );
    Record4& operator=( const IRecord4& rhs );
    void extend( const IRecord4& rhs );
    const IRecordSpecification4& specification() const;
    const IField4& operator[] ( const std::string& name ) const;
    IField4& operator[] ( const std::string& name );
    const IField4& operator[] ( UInt32 index ) const;
    IField4& operator[] ( UInt32 index );
    const AttributeList4& attributeList() const;
  private:
    const IField4& field( UInt32 index ) const;
    IField4& field( UInt32 index );
    void reset();
  private:
    RecordSpecification4 m_rspec;
    AttributeList4 m_attrList;
    std::vector< IField4* > m_fields;
  };

  //---------------------------------------------------------------------------

}
#endif // TEST_ROOT8458G_LIB_H

