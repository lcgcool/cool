#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "test_root8458g_lib.h"

#define NOOP throw std::runtime_error("NOOP!")

namespace rbug
{
  class FieldWrongCppType4 : public std::runtime_error {
  public:
    virtual ~FieldWrongCppType4() throw() {}
    explicit FieldWrongCppType4( const std::string& name,
                                 const StorageType4& storageType,
                                 const std::type_info& templateCppType,
                                 const std::string& domain )
      : std::runtime_error
        ( "Cannot get or set data for field '" + name
          + "' of storage type '" + storageType.name()
          + "' and C++ type '" + storageType.cppType().name()
          + "' using wrong C++ type '" + templateCppType.name()
          + "' from '" + domain + "'" ) {}
  };

  class FieldAdapter4 : public IField4
  {
  public:
    // SIMPLIFIED IMPLEMENTATION OF NEEDED METHODS
    virtual ~FieldAdapter4(){}
    FieldAdapter4( const IFieldSpecification4& fspec ) : m_fspec( fspec ){}
    const IFieldSpecification4& specification() const{ return m_fspec; }
    void setValue( const std::type_info& cppType, const void* )
    {
      if ( this->storageType().cppType() != cppType )
        throw FieldWrongCppType4
          ( this->name(), this->storageType(), cppType, "FieldAdapter" );
    }
    // DUMMY IMPLEMENTATION
    bool isNull() const{ NOOP; }
    const void* addressOfData() const{ NOOP; }
    void setNull(){ NOOP; }
    bool compareValue( const IField4& ) const{ NOOP; }
    std::ostream& printValue( std::ostream& ) const{ NOOP; }
    const Attribute4& attribute() const{ NOOP; }
  private:
    FieldAdapter4();
    FieldAdapter4( const FieldAdapter4& );
    FieldAdapter4& operator=( const FieldAdapter4& );
  private:
    const IFieldSpecification4& m_fspec;
  };

  // SIMPLIFIED IMPLEMENTATION OF NEEDED METHODS
  FieldSpecification4::~FieldSpecification4(){}
  FieldSpecification4::FieldSpecification4( const std::string& name, const StorageType4::TypeId typeId )
    : m_name( name ), m_typeId( typeId ) {
    if ( name == "" ) throw std::runtime_error( "No name provided" );
  }
  const std::string& FieldSpecification4::name() const{ return m_name; }
  const StorageType4& FieldSpecification4::storageType() const{ return StorageType4::storageType( m_typeId ); }

  // DUMMY IMPLEMENTATION
  bool FieldSpecification4::operator==( const IFieldSpecification4& ) const{ NOOP; }
  bool FieldSpecification4::operator!=( const IFieldSpecification4& ) const{ NOOP; }
  void FieldSpecification4::validate( const IField4&, bool ) const{ NOOP; }
  void FieldSpecification4::validate( const Attribute4&, bool ) const{ NOOP; }

  // SIMPLIFIED IMPLEMENTATION OF NEEDED METHODS
  Record4::~Record4() {
    std::cout << "Calling Record4 destructor" << std::endl;
    for ( UInt32 i=0; i<m_fields.size(); i++ ) delete m_fields[i];
  }
  Record4::Record4( const IFieldSpecification4& fspec )
    : m_rspec( new FieldSpecification4( fspec.name(), fspec.storageType().id() ) )
    , m_attrList(), m_fields() {
    std::cout << "Calling Record4 constructor( IFieldSpec& )" << std::endl;
    //const std::string& name = fspec.name();
    //const StorageType4& type = fspec.storageType();
    //m_rspec.extend( name, type.id() );
    FieldAdapter4* field = new FieldAdapter4( *(m_rspec.m_pfspec) );
    m_fields.push_back( field );
  }
  IField4& Record4::operator[] ( UInt32 index ) {
    std::cout << "Calling Record4::operator[ int ] non-const" << std::endl;
    if ( index >= m_fields.size() )
      throw std::runtime_error("Index out of range");
    return *(m_fields[index]);
  }

  // DUMMY IMPLEMENTATION
  Record4::Record4(){ NOOP; }
  Record4::Record4( const IRecordSpecification4& ){ NOOP; }
  Record4::Record4( const Record4& ){ NOOP; }
  Record4::Record4( const IRecord4& ){ NOOP; }
  Record4::Record4( const IRecordSpecification4&, const AttributeList4& ){ NOOP; }
  Record4& Record4::operator=( const Record4& ){ NOOP; }
  Record4& Record4::operator=( const IRecord4& ){ NOOP; }
  const IField4& Record4::operator[] ( const std::string& ) const{ NOOP; }
  IField4& Record4::operator[] ( const std::string& ){ NOOP; }
  const IField4& Record4::operator[] ( UInt32 ) const{ NOOP; }
  IField4& Record4::field( UInt32 ){ NOOP; }
  const IField4& Record4::field( UInt32 ) const{ NOOP; }
  const AttributeList4& Record4::attributeList() const{ NOOP; }
  void Record4::reset(){ NOOP; }
  void Record4::extend( const IRecord4& ){ NOOP; }
  const IRecordSpecification4& Record4::specification() const{ NOOP; }

  // SIMPLIFIED IMPLEMENTATION OF NEEDED METHODS
  const std::string StorageType4::name() const
  {
    switch ( m_id ) {
    case StorageType4::TypeId::Bool:      return "Bool";
    case StorageType4::TypeId::UChar:     return "UChar";
    case StorageType4::TypeId::Int16:     return "Int16";
    case StorageType4::TypeId::UInt16:    return "UInt16";
    case StorageType4::TypeId::Int32:     return "Int32";
    case StorageType4::TypeId::UInt32:    return "UInt32";
    case StorageType4::TypeId::UInt63:    return "UInt63";
    case StorageType4::TypeId::Int64:     return "Int64";
    case StorageType4::TypeId::Float:     return "Float";
    case StorageType4::TypeId::Double:    return "Double";
    case StorageType4::TypeId::String255: return "String255";
    case StorageType4::TypeId::String4k:  return "String4k";
    case StorageType4::TypeId::String64k: return "String64k";
    case StorageType4::TypeId::String16M: return "String16M";
    case StorageType4::TypeId::String128M: return "String128M";
    case StorageType4::TypeId::Blob64k:   return "Blob64k";
    case StorageType4::TypeId::Blob16M:   return "Blob16M";
    case StorageType4::TypeId::Blob128M:   return "Blob128M";
    }
    std::stringstream out;
    out << "PANIC! Unknown type '" << m_id << "' in StorageType4::name()";
    throw std::runtime_error( out.str()+" StorageType4" );
  }

  const std::type_info& StorageType4::cppType() const
  {
    switch ( m_id ) {
    case StorageType4::TypeId::Bool:      return typeid( rbug::Bool );
    case StorageType4::TypeId::UChar:     return typeid( rbug::UChar );
    case StorageType4::TypeId::Int16:     return typeid( rbug::Int16 );
    case StorageType4::TypeId::UInt16:    return typeid( rbug::UInt16 );
    case StorageType4::TypeId::Int32:     return typeid( rbug::Int32 );
    case StorageType4::TypeId::UInt32:    return typeid( rbug::UInt32 );
    case StorageType4::TypeId::UInt63:    return typeid( rbug::UInt63 );
    case StorageType4::TypeId::Int64:     return typeid( rbug::Int64 );
    case StorageType4::TypeId::Float:     return typeid( rbug::Float );
    case StorageType4::TypeId::Double:    return typeid( rbug::Double );
    case StorageType4::TypeId::String255: return typeid( rbug::String255 );
    case StorageType4::TypeId::String4k:  return typeid( rbug::String4k );
    case StorageType4::TypeId::String64k: return typeid( rbug::String64k );
    case StorageType4::TypeId::String16M: return typeid( rbug::String16M );
    case StorageType4::TypeId::String128M: return typeid( rbug::String128M );
    case StorageType4::TypeId::Blob64k:   return typeid( rbug::Blob64k );
    case StorageType4::TypeId::Blob16M:   return typeid( rbug::Blob16M );
    case StorageType4::TypeId::Blob128M:   return typeid( rbug::Blob128M );
    }
    std::stringstream out;
    out << "PANIC! Unknown type '" << m_id << "' in StorageType4::cppType()";
    throw std::runtime_error( out.str()+" StorageType4" );
  }

  const StorageType4& StorageType4::storageType( const TypeId& id )
  {
    switch ( id ) {
    case StorageType4::TypeId::Bool:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::UChar:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::Int16:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::UInt16:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::Int32:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::UInt32:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::UInt63:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::Int64:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::Float:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::Double:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::String255:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::String4k:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::String64k:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::String16M:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::String128M:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::Blob64k:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::Blob16M:
      { static const StorageType4 type( id ); return type; }
    case StorageType4::TypeId::Blob128M:
      { static const StorageType4 type( id ); return type; }
    }
    std::stringstream out;
    out << "PANIC! Unknown type '" << id << "' in StorageType4::storageType()";
    throw std::runtime_error( out.str()+" StorageType4" );
  }

  // DUMMY IMPLEMENTATION
  size_t StorageType4::maxSize() const{ NOOP; }
  void StorageType4::validate( const std::type_info&, const void*, const std::string& ) const{ NOOP; }

}
