# See ROOT-6397 and CORALCOOL-2233
import os
dir=os.path.dirname( os.path.realpath(__file__) )

# Dump the compiler version
print '=== DUMP COMPILER VERSION'
os.system("cd "+dir+"; $CXX -v")
print

# Test compilation in c++98
print '=== TEST COMPILATION WITH c++98'
os.system("cd "+dir+"; $CXX -std=c++98 -o test_root6397.o -c test_root6397.cpp")
print

# Test compilation in c++11
print '=== TEST COMPILATION WITH c++11'
os.system("cd "+dir+"; $CXX -std=c++11 -o test_root6397.o -c test_root6397.cpp")
print

# Test compilation in c++11 including the FAILS sections
print '=== TEST COMPILATION WITH c++11 including the FAILS section'
os.system("cd "+dir+"; $CXX -DFAILS=1 -std=c++11 -o test_root6397.o -c test_root6397.cpp")
print

# Compile and test in ROOT
print '=== TEST COMPILATION IN ROOT'
import ROOT
ROOT.gInterpreter.ProcessLine("#include \""+dir+"/test_root6397.cpp\"")
print
print '=== TEST EXECUTION IN ROOT'
cool = ROOT.cool
cool.dump()
def tryeval( expr ):
    try: print expr, eval(expr)
    except: print "[NOT YET IN ROOT (ROOT-6397)]"    
tryeval( 'cool.PayloadMode.SEPARATEPAYLOAD' )
tryeval( 'cool.PayloadMode.Mode.SEPARATEPAYLOAD' )
tryeval( 'cool.PayloadMode2.Mode.SEPARATEPAYLOAD' )
tryeval( 'cool.PayloadMode2.SEPARATEPAYLOAD' ) # not ok in c++!
print
tryeval( 'cool.StorageType.UInt32' )
tryeval( 'cool.StorageType.TypeId.UInt32' )
tryeval( 'cool.StorageType2.TypeId.UInt32' )
tryeval( 'cool.StorageType2.UInt32' ) # not ok in c++!
