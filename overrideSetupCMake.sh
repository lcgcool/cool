CORAL_base=
if [ "$USER" == "gitlab-runner" ]; then
  CORAL_base=./coral
elif [ "$USER" == "mavogel" ]; then
  CORAL_base=/data/mavogel/git/coral
fi
export CMAKE_PREFIX_PATH=$CORAL_base/$BINARY_TAG:$CMAKE_PREFIX_PATH
